/*
 * name -> nome
 * image -> nome do arquivo que está na pasta src/assets/images/projects/
 * program -> nome do programa
 * theme -> tema
 * complexit -> complexidade
 * ODS -> objetivos sustentavéis
 * status -> qual o status do projeto
 * begin -> data do inicio do projeto
 * end -> data do término do projeto
 * subject -> sujeito de ação
 * volunteers -> número de voluntários
 * benefited -> número de beneficiados
 * mapLink -> link do google maps
 * videoLink -> link do video no youtube
 * coordinatorsList -> lista de coordenadores
*/

export default {
  name: '',
  image: '',
  program: '',
  theme: '',
  complexit: '',
  ODS: [
    '',
    '',
    '',
  ],
  status: '',
  begin: '',
  end: '',
  subject: '',
  resources: '',
  volunteers: '',
  benefited: '',
  mapLink: '',
  videoLink: '',
  coordinatorsList: [],
};
