/*********************************************************************
 *                                                                   *
 * name -> nome, tem que estar entre aspas simples                   *
 * program -> programa, tem que estar entre aspas simples            *
 * image -> nome do arquivo que está na pasta assets/images/members/ *
 *                                                                   *
 *********************************************************************/

export default {
  name: 'Júlia Oliveira',
  program: 'Lazer e Turismo',
  image: 'julia.jpg',
};
