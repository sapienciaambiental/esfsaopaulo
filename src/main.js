import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faAngleRight,
  faUsers,
  faChild,
  faMapMarked,
  faHandsHelping,
  faBullhorn,
  faComments,
  faHandRock,
  faHammer,
  faLaugh
} from '@fortawesome/free-solid-svg-icons';

import {
  faYoutube,
  faFacebookF,
  faInstagram,
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons';

import VueYouTubeEmbed from 'vue-youtube-embed';

Vue.use(VueYouTubeEmbed);

library.add(
  faAngleRight,
  faUsers,
  faChild,
  faMapMarked,
  faHandsHelping,
  faBullhorn,
  faHandRock,
  faHammer,
  faLaugh,
  faComments,
  faYoutube,
  faFacebookF,
  faInstagram,
  faLinkedinIn
);

Vue.component('fas', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
